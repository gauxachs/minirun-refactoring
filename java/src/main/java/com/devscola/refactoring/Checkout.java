package com.devscola.refactoring;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Checkout {

    private List<String> items;
    private List<String> items2;
    private Integer result;
    private PricingRules rules;

    public Checkout(PricingRules rules) {
        items = new ArrayList<>();
        items2 = new ArrayList<>();
        result = 0;
        this.rules = rules;
    }

    public void scan(String items) {
        this.items = Arrays.asList(items.split(""));
    }

    public Integer total() {
        items.forEach(e -> {
            items2.add(e);
            result += rules.getTable().get(e);
            var i = 0;
            for (String ee : items2) { if (ee.equals(e)) i += 1;}
            result -= (e.equals("A") && i % 3 == 0) ? 20 : ((e.equals("B") && i % 2 == 0) ? 15 : 0);
        });
        return this.result;
    }

}
